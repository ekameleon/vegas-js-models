/**
 * A library who contains classes and tools that provides extra <code>models</code> methods and implementations - version: 1.0.1 - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global.vegas_models = factory());
}(this, (function () { 'use strict';

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};
      var ownKeys = Object.keys(source);

      if (typeof Object.getOwnPropertySymbols === 'function') {
        ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
          return Object.getOwnPropertyDescriptor(source, sym).enumerable;
        }));
      }

      ownKeys.forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    }

    return target;
  }

  class Receiver
  {
      constructor()
      {
      }
      receive() {}
      toString()
      {
          return '[' + this.constructor.name + ']' ;
      }
  }

  class Signaler
  {
      constructor()
      {
      }
      get length()
      {
          return 0 ;
      }
      connect( receiver , priority = 0 , autoDisconnect = false )
      {
      }
      connected()
      {
      }
      disconnect( receiver = null ) {} ;
      emit( ...values )
      {
      }
      hasReceiver( receiver )
      {
          return false ;
      }
      toString()
      {
          return '[Signaler]' ;
      }
  }

  class Signal extends Signaler
  {
      constructor()
      {
          super() ;
          Object.defineProperties( this ,
          {
              proxy : { value : null, configurable : true , writable : true } ,
              receivers : { writable : true , value : [] }
          }) ;
      }
      get length()
      {
          return this.receivers.length ;
      }
      connect( receiver , priority = 0 , autoDisconnect = false )
      {
          if ( receiver === null )
          {
              return false ;
          }
          autoDisconnect = autoDisconnect === true ;
          priority = priority > 0 ? (priority - (priority % 1)) : 0 ;
          if ( ( typeof(receiver) === "function" ) || ( receiver instanceof Function ) || ( receiver instanceof Receiver ) || ( "receive" in receiver ) )
          {
              if ( this.hasReceiver( receiver ) )
              {
                  return false ;
              }
              this.receivers.push( new SignalEntry( receiver , priority , autoDisconnect ) ) ;
              let i ;
              let j ;
              let a = this.receivers ;
              let swap = function( j , k )
              {
                  let temp = a[j] ;
                  a[j]     = a[k] ;
                  a[k]     = temp ;
                  return true ;
              };
              let swapped = false;
              let l = a.length ;
              for( i = 1 ; i < l ; i++ )
              {
                  for( j = 0 ; j < ( l - i ) ; j++ )
                  {
                      if ( a[j+1].priority > a[j].priority )
                      {
                          swapped = swap(j, j+1) ;
                      }
                  }
                  if ( !swapped )
                  {
                      break;
                  }
              }
              return true ;
          }
          return false ;
      }
      connected()
      {
          return this.receivers.length > 0 ;
      }
      disconnect( receiver = null )
      {
          if ( receiver === null )
          {
              if ( this.receivers.length > 0 )
              {
                  this.receivers = [] ;
                  return true ;
              }
              else
              {
                  return false ;
              }
          }
          if ( this.receivers.length > 0 )
          {
              let l = this.receivers.length ;
              while( --l > -1 )
              {
                  if ( this.receivers[l].receiver === receiver )
                  {
                      this.receivers.splice( l , 1 ) ;
                      return true ;
                  }
              }
          }
          return false ;
      }
      emit( ...values )
      {
          let l = this.receivers.length ;
          if ( l === 0 )
          {
              return ;
          }
          let i ;
          let r = [] ;
          let a = this.receivers.slice() ;
          let e ;
          let slot ;
          for ( i = 0 ; i < l ; i++ )
          {
              e = a[i] ;
              if ( e.auto )
              {
                  r.push( e )  ;
              }
          }
          if ( r.length > 0 )
          {
              l = r.length ;
              while( --l > -1 )
              {
                  i = this.receivers.indexOf( r[l] ) ;
                  if ( i > -1 )
                  {
                      this.receivers.splice( i , 1 ) ;
                  }
              }
          }
          l = a.length ;
          for ( i = 0 ; i<l ; i++ )
          {
              slot = a[i].receiver ;
              if( slot instanceof Function || typeof(slot) === "function" )
              {
                  slot.apply( this.proxy || this , values ) ;
              }
              else if ( slot instanceof Receiver || ( "receive" in slot && (slot.receive instanceof Function) ) )
              {
                  slot.receive.apply( this.proxy || slot , values ) ;
              }
          }
      }
      hasReceiver( receiver )
      {
          if ( receiver === null )
          {
              return false ;
          }
          if ( this.receivers.length > 0 )
          {
              let l = this.receivers.length ;
              while( --l > -1 )
              {
                  if ( this.receivers[l].receiver === receiver )
                  {
                      return true ;
                  }
              }
          }
          return false ;
      }
      toArray()
      {
          if ( this.receivers.length > 0 )
          {
              return this.receivers.map( item => item.receiver ) ;
          }
          return [] ;
      }
      toString()
      {
          return '[Signal]' ;
      }
  }
  class SignalEntry
  {
      constructor( receiver , priority = 0 , auto = false )
      {
          this.auto = auto ;
          this.receiver = receiver ;
          this.priority = priority ;
      }
      toString () { return '[SignalEntry]' ; }
  }

  function Lockable()
  {
      Object.defineProperties( this ,
      {
          __lock__ : { writable : true  , value : false }
      }) ;
  }
  Lockable.prototype = Object.create( Object.prototype ,
  {
      constructor : { writable : true , value : Lockable } ,
      isLocked : { writable : true , value : function()
      {
          return this.__lock__ ;
      }},
      lock : { writable : true , value : function()
      {
          this.__lock__ = true ;
      }},
      unlock : { writable : true , value : function()
      {
          this.__lock__ = false ;
      }},
      toString : { writable : true , value : function()
      {
          return '[' + this.constructor.name + ']' ;
      }}
  });

  function Model() {
    Lockable.call(this);
  }
  Model.prototype = Object.create(Lockable.prototype, {
    constructor: {
      writable: true,
      value: Model
    },
    supports: {
      writable: true,
      value: function value(_value) {
        return _value === _value;
      }
    },
    toString: {
      writable: true,
      value: function value() {
        return '[' + this.constructor.name + ']';
      }
    },
    validate: {
      writable: true,
      value: function value(_value2)
      {
        if (!this.supports(_value2)) {
          throw new Error(this + " validate(" + _value2 + ") is mismatch.");
        }
      }
    }
  });

  function ChangeModel() {
    Model.call(this);
    Object.defineProperties(this, {
      beforeChanged: {
        value: new Signal()
      },
      changed: {
        value: new Signal()
      },
      cleared: {
        value: new Signal()
      },
      security: {
        value: true,
        writable: true
      },
      _current: {
        value: null,
        writable: true
      }
    });
  }
  ChangeModel.prototype = Object.create(Model.prototype, {
    constructor: {
      writable: true,
      value: ChangeModel
    },
    current: {
      get: function get() {
        return this._current;
      },
      set: function set(o) {
        if (o === this._current && this.security) {
          return;
        }
        if (o) {
          this.validate(o);
        }
        if (this._current) {
          this.notifyBeforeChange(this._current);
        }
        this._current = o;
        if (this._current) {
          this.notifyChange(this._current);
        }
      }
    },
    clear: {
      writable: true,
      value: function value() {
        this._current = null;
        this.notifyClear();
      }
    },
    notifyBeforeChange: {
      value: function value(_value) {
        if (!this.isLocked()) {
          this.beforeChanged.emit(_value, this);
        }
      }
    },
    notifyChange: {
      value: function value(_value2) {
        if (!this.isLocked()) {
          this.changed.emit(_value2, this);
        }
      }
    },
    notifyClear: {
      value: function value() {
        if (!this.isLocked()) {
          this.cleared.emit(this);
        }
      }
    }
  });

  function NoSuchElementError( message , fileName , lineNumber )
  {
      this.name = 'NoSuchElementError';
      this.message    = message || 'no such element error' ;
      this.fileName   = fileName ;
      this.lineNumber = lineNumber ;
      this.stack      = (new Error()).stack;
  }
  NoSuchElementError.prototype = Object.create( Error.prototype );
  NoSuchElementError.prototype.constructor = NoSuchElementError;

  function MemoryModel() {
    ChangeModel.call(this);
    Object.defineProperties(this, {
      enableErrorChecking: {
        writable: true,
        value: false
      },
      header: {
        value: new MemoryEntry(),
        writable: true
      },
      _reduced: {
        value: false,
        writable: true
      },
      size: {
        value: 0,
        writable: true
      }
    });
    this.header.next = this.header.previous = this.header;
  }
  MemoryModel.prototype = Object.create(ChangeModel.prototype, {
    constructor: {
      writable: true,
      value: MemoryModel
    },
    current: {
      get: function get() {
        return this._current;
      },
      set: function set(o) {
        if (o === this._current && this.security) {
          return;
        }
        if (o) {
          this.validate(o);
        }
        if (this._current) {
          this.notifyBeforeChange(this._current);
        }
        this._current = o;
        if (this._current) {
          this.add(this._current);
          this.notifyChange(this._current);
        }
      }
    },
    length: {
      get: function get() {
        return this.size;
      }
    },
    reduced: {
      get: function get() {
        return this._reduced;
      }
    },
    back: {
      value: function value() {
        var old = this.last();
        if (old) {
          this._reduced = true;
          this.notifyBeforeChange(old);
          this._reduced = false;
        }
        this.removeLast();
        this._current = this.last();
        if (this._current) {
          this.notifyChange(this._current);
        }
        return old;
      }
    },
    backTo: {
      value: function value() {
        var pos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
        if (pos < 1) {
          pos = 1;
        }
        if (this.size > 1) {
          if (pos < this.size) {
            this._reduced = true;
            var old = this.last();
            if (old) {
              this.notifyBeforeChange(old);
            }
            while (pos !== this.size) {
              this.removeLast();
            }
            this._reduced = false;
            this._current = this.last();
            if (this._current) {
              this.notifyChange(this._current);
            }
            return old;
          } else {
            if (this.enableErrorChecking) {
              throw new RangeError(this + " backTo failed, the passed-in index '" + pos + "' is out of bounds (" + this.size + ".");
            } else {
              return null;
            }
          }
        } else {
          if (this.enableErrorChecking) {
            throw new NoSuchElementError(this + " backTo failed, the length of the memory model must be greater than 1 element.");
          } else {
            return null;
          }
        }
      }
    },
    clear: {
      value: function value() {
        if (this.size > 0) {
          var e = this.header.next;
          var next;
          while (e !== this.header) {
            next = e.next;
            e.next = e.previous = null;
            e.element = null;
            e = next;
          }
          this.header.next = this.header.previous = this.header;
          this.size = 0;
        }
        ChangeModel.prototype.clear.call(this);
      }
    },
    first: {
      value: function value() {
        if (this.size > 0) {
          return this.header.next.element;
        } else {
          if (this.enableErrorChecking) {
            throw new NoSuchElementError(this + " first method failed, the memory is empty.");
          } else {
            return null;
          }
        }
      }
    },
    home: {
      value: function value() {
        if (this.size > 1) {
          var old = this.header.previous.element;
          if (old) {
            this.notifyBeforeChange(old);
          }
          var top = this.header.next;
          while (this.header.previous !== top) {
            this.removeEntry(this.header.previous);
          }
          this._current = this.last();
          if (this._current) {
            this.notifyChange(this._current);
          }
          return old;
        } else {
          if (this.enableErrorChecking) {
            throw new NoSuchElementError(this + " home failed, the length of the memory model must be greater than 1 element.");
          } else {
            return null;
          }
        }
      }
    },
    isEmpty: {
      value: function value() {
        return this.size === 0;
      }
    },
    last: {
      value: function value() {
        if (this.size > 0) {
          return this.header.previous.element;
        } else {
          if (this.enableErrorChecking) {
            throw new NoSuchElementError(this + " last method failed, the memory is empty.");
          } else {
            return null;
          }
        }
      }
    },
    add: {
      value: function value(element) {
        this.addBefore(element, this.header);
        return element;
      }
    },
    addBefore: {
      value: function value(element, entry) {
        var e = new MemoryEntry(element, entry, entry.previous);
        e.previous.next = e;
        e.next.previous = e;
        this.size++;
        return e;
      }
    },
    removeEntry: {
      value: function value(entry) {
        if (entry === this.header) {
          if (this.enableErrorChecking) {
            throw new NoSuchElementError(this + " removeEntry failed.");
          } else {
            return null;
          }
        }
        var result = entry.element;
        entry.previous.next = entry.next;
        entry.next.previous = entry.previous;
        entry.next = entry.previous = null;
        entry.element = null;
        this.size--;
        return result;
      }
    },
    removeLast: {
      value: function value() {
        return this.removeEntry(this.header.previous);
      }
    }
  });
  function MemoryEntry() {
    var element = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var next
    = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var previous
    = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    this.element = element;
    this.next = next;
    this.previous = previous;
  }

  function ArrayModel() {
    var factory = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    ChangeModel.call(this);
    Object.defineProperties(this, {
      added: {
        value: new Signal()
      },
      removed: {
        value: new Signal()
      },
      updated: {
        value: new Signal()
      },
      _array: {
        writable: true,
        value: factory instanceof Array ? factory : []
      }
    });
  }
  ArrayModel.prototype = Object.create(ChangeModel.prototype, {
    constructor: {
      writable: true,
      value: ArrayModel
    },
    length: {
      get: function get() {
        return this._array.length;
      }
    },
    add: {
      value: function value(entry) {
        if (entry === null || entry === undefined) {
          throw new ReferenceError(this + " add method failed, the passed-in argument not must be 'null'.");
        }
        this.validate(entry);
        this._array.push(entry);
        this.notifyAdd(this._array.length - 1, entry);
      }
    },
    addAt: {
      value: function value(index, entry) {
        if (entry === null || entry === undefined) {
          throw new ReferenceError(this + " add method failed, the passed-in argument not must be 'null'.");
        }
        this.validate(entry);
        this._array.splice(index, 0, entry);
        this.notifyAdd(index, entry);
      }
    },
    clear: {
      value: function value() {
        this._array.length = 0;
        ChangeModel.prototype.clear.call(this);
      }
    },
    get: {
      value: function value(index) {
        return this._array[index];
      }
    },
    has: {
      value: function value(entry) {
        return this._array.indexOf(entry) > -1;
      }
    },
    isEmpty: {
      value: function value() {
        return this._array.length === 0;
      }
    },
    notifyAdd: {
      value: function value(index, entry) {
        if (!this.isLocked()) {
          this.added.emit(index, entry, this);
        }
      }
    },
    notifyRemove: {
      value: function value(index, entry) {
        if (!this.isLocked()) {
          this.removed.emit(index, entry, this);
        }
      }
    },
    notifyUpdate: {
      value: function value(index, entry) {
        if (!this.isLocked()) {
          this.updated.emit(index, entry, this);
        }
      }
    },
    remove: {
      value: function value(entry) {
        if (entry === null || entry === undefined) {
          throw new ReferenceError(this + " remove method failed, the entry passed in argument not must be null.");
        }
        var index = this._array.indexOf(entry);
        if (index > -1) {
          this.removeAt(index);
        } else {
          throw new ReferenceError(this + " remove method failed, the entry is not register in the model.");
        }
      }
    },
    removeAt: {
      value: function value(index) {
        var count = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
        count = count > 1 ? count : 1;
        var old = this._array.splice(index, count);
        if (old) {
          this.notifyRemove(index, old);
        }
      }
    },
    removeRange: {
      value: function value(fromIndex, toIndex) {
        if (fromIndex === toIndex) {
          return null;
        }
        return this.removeAt(fromIndex, toIndex - fromIndex);
      }
    },
    setArray: {
      value: function value(ar) {
        this._array = ar instanceof Array ? ar : [];
      }
    },
    updateAt: {
      value: function value(index, entry) {
        this.validate(entry);
        var old = this._array[index];
        if (old) {
          this._array[index] = entry;
          this.notifyUpdate(index, old);
        }
      }
    },
    toArray: {
      value: function value() {
        return this._array;
      }
    }
  });

  class Runnable
  {
      run()
      {
      }
      toString()
      {
          return '[' + this.constructor.name + ']' ;
      }
  }

  const TaskPhase = Object.defineProperties( {} ,
  {
      ERROR : { value : 'error' , enumerable : true } ,
      DELAYED : { value : 'delayed'  , enumerable : true } ,
      FINISHED : { value : 'finished' , enumerable : true } ,
      INACTIVE : { value : 'inactive' , enumerable : true } ,
      RUNNING : { value : 'running' , enumerable : true } ,
      STOPPED : { value : 'stopped' , enumerable : true } ,
      TIMEOUT : { value : 'timeout' , enumerable : true }
  }) ;

  class Action extends Runnable
  {
      constructor()
      {
          super() ;
          Object.defineProperties( this ,
          {
              finishIt : { value : new Signal() } ,
              startIt : { value : new Signal() } ,
              __lock__ : { writable : true  , value : false },
              _phase : { writable : true , value : TaskPhase.INACTIVE },
              _running : { writable : true , value : false }
          }) ;
      }
      get phase () { return this._phase ; }
      get running() { return this._running ; }
      clone()
      {
          return new Action() ;
      }
      isLocked()
      {
          return this.__lock__ ;
      }
      lock()
      {
          this.__lock__ = true ;
      }
      notifyFinished()
      {
          this._running = false ;
          this._phase = TaskPhase.FINISHED ;
          this.finishIt.emit( this ) ;
          this._phase = TaskPhase.INACTIVE ;
      }
      notifyStarted()
      {
          this._running = true ;
          this._phase  = TaskPhase.RUNNING ;
          this.startIt.emit( this ) ;
      }
      unlock()
      {
          this.__lock__ = false ;
      }
  }

  function KeyValuePair() {}
  KeyValuePair.prototype = Object.create( Object.prototype ,
  {
      constructor : { writable : true , value :  KeyValuePair },
      length : { get : () => 0 } ,
      clear : { value : function () {} , writable : true } ,
      clone : { value : function () { return new KeyValuePair() } , writable : true } ,
      copyFrom : { value : function( map ) {} , writable : true } ,
      delete : { value : function( key ) {} , writable : true } ,
      forEach : { value : function( callback , thisArg = null ) {} , writable : true } ,
      get : { value : function( key ) { return null ; } , writable : true } ,
      has : { value : function( key ) { return false ; } , writable : true } ,
      hasValue : { value : function( value ) { return false ; } , writable : true } ,
      isEmpty : { value : function() { return false ; } , writable : true } ,
      iterator : { value : function()              { return null ; } , writable : true } ,
      keyIterator : { value : function()              { return null } , writable : true } ,
      keys : { value : function() { return null ; } , writable : true } ,
      set : { value : function( key , value ) {} , writable : true } ,
      toString : { value : function () { return '[' + this.constructor.name + ']' ; } , writable : true } ,
      values : { value : function ()  {} , writable : true }
  }) ;

  function MapFormatter() {}
  MapFormatter.prototype = Object.create( Object.prototype ,
  {
      constructor : { writable : true , value : MapFormatter },
      format : { value : function( value )
      {
          if ( value instanceof KeyValuePair )
          {
              let r = "{";
              let keys = value.keys()   ;
              let len  = keys.length ;
              if( len > 0 )
              {
                  let values = value.values() ;
                  for( let i = 0 ; i < len ; i++ )
                  {
                      r += keys[i] + ':' + values[i] ;
                      if( i < len - 1 )
                      {
                          r += "," ;
                      }
                  }
              }
              r += "}" ;
              return r ;
          }
          else
          {
              return "{}" ;
          }
      }}
  }) ;

  const formatter = new MapFormatter() ;

  function MapEntry( key , value )
  {
      Object.defineProperties( this ,
      {
          key : { value : key , writable : true },
          value : { value : value , writable : true }
      }) ;
  }
  MapEntry.prototype = Object.create( Object.prototype ,
  {
      constructor : { value : MapEntry } ,
      clone : { value : function()
      {
          return new MapEntry( this.key , this.value ) ;
      }},
      toString : { value : function()
      {
          return "[MapEntry key:" + this.key + " value:" + this.value + "]" ;
      }}
  }) ;

  function Iterator() {}
  Iterator.prototype = Object.create( Object.prototype ,
  {
      constructor : { writable : true , value : Iterator } ,
      delete : { writable : true , value : function() {} } ,
      hasNext : { writable : true , value : function() {} } ,
      key : { writable : true , value : function() {} } ,
      next : { writable : true , value : function() {} } ,
      reset : { writable : true , value : function() {} } ,
      seek : { writable : true , value : function ( position ) {} } ,
      toString :
      {
          writable : true , value : function ()
          {
              return '[' + this.constructor.name + ']' ;
          }
      }
  }) ;

  function ArrayIterator( array )
  {
      if ( !(array instanceof Array) )
      {
          throw new ReferenceError( this + " constructor failed, the passed-in Array argument not must be 'null'.") ;
      }
      Object.defineProperties( this ,
      {
          _a : { value : array , writable : true } ,
          _k : { value : -1    , writable : true }
      }) ;
  }
  ArrayIterator.prototype = Object.create( Iterator.prototype ,
  {
      constructor : { value : ArrayIterator } ,
      delete : { value : function()
      {
          return this._a.splice(this._k--, 1)[0] ;
      }},
      hasNext : { value : function()
      {
          return (this._k < this._a.length -1) ;
      }},
      key : { value : function()
      {
          return this._k ;
      }},
      next : { value : function()
      {
          return this._a[++this._k] ;
      }},
      reset : { value : function()
      {
          this._k = -1 ;
      }},
      seek : { value : function ( position )
      {
          position = Math.max( Math.min( position - 1 , this._a.length ) , -1 ) ;
          this._k = isNaN(position) ? -1 : position ;
      }}
  }) ;

  function MapIterator( map )
  {
      if ( map && ( map instanceof KeyValuePair) )
      {
          Object.defineProperties( this ,
          {
              _m : { value : map  , writable : true } ,
              _i : { value : new ArrayIterator( map.keys() ) , writable : true } ,
              _k : { value : null , writable : true }
          }) ;
      }
      else
      {
         throw new ReferenceError( this + " constructor failed, the passed-in KeyValuePair argument not must be 'null'.") ;
      }
  }
  MapIterator.prototype = Object.create( Iterator.prototype ,
  {
      constructor : { writable : true , value : MapIterator } ,
      delete : { value : function()
      {
          this._i.delete() ;
          return this._m.delete( this._k ) ;
      }},
      hasNext : { value : function()
      {
          return this._i.hasNext() ;
      }},
      key : { value : function()
      {
          return this._k ;
      }},
      next : { value : function()
      {
          this._k = this._i.next() ;
          return this._m.get( this._k ) ;
      }},
      reset : { value : function()
      {
          this._i.reset() ;
      }},
      seek : { value : function ( position )
      {
          throw new Error( "This Iterator does not support the seek() method.") ;
      }}
  }) ;

  function ArrayMap( keys = null , values = null )
  {
      Object.defineProperties( this ,
      {
          _keys :
          {
              value : [] ,
              writable : true
          },
          _values :
          {
              value : [] ,
              writable : true
          }
      }) ;
      if ( keys === null || values === null )
      {
          this._keys   = [] ;
          this._values = [] ;
      }
      else
      {
          let b = ( keys instanceof Array && values instanceof Array && keys.length > 0 && keys.length === values.length ) ;
          this._keys   = b ? [].concat(keys)   : [] ;
          this._values = b ? [].concat(values) : [] ;
      }
  }
  ArrayMap.prototype = Object.create( KeyValuePair.prototype ,
  {
      constructor : { writable : true , value : ArrayMap } ,
      length : { get : function() { return this._keys.length ; } } ,
      clear : { value : function ()
      {
          this._keys   = [] ;
          this._values = [] ;
      }},
      clone : { value : function ()
      {
          return new ArrayMap( this._keys , this._values ) ;
      }},
      copyFrom : { value : function ( map )
      {
          if ( !map || !(map instanceof KeyValuePair) )
          {
              return ;
          }
          let keys = map.keys() ;
          let values = map.values() ;
          let l = keys.length ;
          for ( let i = 0 ; i<l ; i = i - (-1) )
          {
              this.set(keys[i], values[i]) ;
          }
      }},
      delete : { value : function ( key )
      {
          let v = null ;
          let i = this.indexOfKey( key ) ;
          if ( i > -1 )
          {
              v = this._values[i] ;
              this._keys.splice(i, 1) ;
              this._values.splice(i, 1) ;
          }
          return v ;
      }},
      forEach : { value : function( callback , thisArg = null )
      {
          if (typeof callback !== "function")
          {
              throw new TypeError( callback + ' is not a function' );
          }
          let l = this._keys.length ;
          for( let i = 0 ; i<l ; i++ )
          {
              callback.call( thisArg , this._values[i] , this._keys[i] , this ) ;
          }
      }},
      get : { value : function( key )
      {
          return this._values[ this.indexOfKey( key ) ] ;
      }},
      getKeyAt : { value : function ( index )
      {
          return this._keys[ index ] ;
      }},
      getValueAt : { value : function ( index          )
      {
          return this._values[ index ] ;
      }},
      has : { value : function ( key )
      {
          return this.indexOfKey(key) > -1 ;
      }},
      hasValue : { value : function ( value )
      {
          return this.indexOfValue( value ) > -1 ;
      }},
      indexOfKey : { value : function (key)
      {
          let l = this._keys.length ;
          while( --l > -1 )
          {
              if ( this._keys[l] === key )
              {
                  return l ;
              }
          }
          return -1 ;
      }},
      indexOfValue : { value : function (value)
      {
          let l = this._values.length ;
          while( --l > -1 )
          {
              if ( this._values[l] === value )
              {
                  return l ;
              }
          }
          return -1 ;
      }},
      isEmpty : { value : function ()
      {
          return this._keys.length === 0 ;
      }},
      iterator : { value : function ()
      {
          return new MapIterator( this ) ;
      }},
      keyIterator : { value : function ()
      {
          return new ArrayIterator( this._keys ) ;
      }},
      keys : { value : function ()
      {
          return this._keys.concat() ;
      }},
      set : { value : function ( key , value )
      {
          let r = null ;
          let i = this.indexOfKey( key ) ;
          if ( i < 0 )
          {
              this._keys.push( key ) ;
              this._values.push( value ) ;
          }
          else
          {
              r = this._values[i] ;
              this._values[i] = value ;
          }
          return r ;
      }},
      setKeyAt : { value : function( index , key )
      {
          if ( index >= this._keys.length )
          {
              throw new RangeError( "ArrayMap.setKeyAt(" + index + ") failed with an index out of the range.") ;
          }
          if ( this.containsKey( key ) )
          {
              return null ;
          }
          let k = this._keys[index] ;
          if (k === undefined)
          {
              return null ;
          }
          let v = this._values[index] ;
          this._keys[index] = key ;
          return new MapEntry( k , v ) ;
      }},
      setValueAt : { value : function( index , value )
      {
          if ( index >= this._keys.length )
          {
              throw new RangeError( "ArrayMap.setValueAt(" + index + ") failed with an index out of the range.") ;
          }
          let v = this._values[index] ;
          if (v === undefined)
          {
              return null ;
          }
          let k = this._keys[index] ;
          this._values[index] = value ;
          return new MapEntry( k , v ) ;
      }},
      toString : { value : function () { return formatter.format( this ) ; }},
      values : { value : function ()
      {
          return this._values.concat() ;
      }}
  }) ;

  function MapModel() {
    var factory = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "id";
    ChangeModel.call(this);
    Object.defineProperties(this, {
      added: {
        value: new Signal()
      },
      removed: {
        value: new Signal()
      },
      updated: {
        value: new Signal()
      },
      _map: {
        writable: true,
        value: factory instanceof KeyValuePair ? factory : new ArrayMap()
      },
      _primaryKey: {
        value: !(key instanceof String || typeof key === 'string') || key === "" ? MapModel.DEFAULT_PRIMARY_KEY : key,
        writable: true
      }
    });
  }
  Object.defineProperty(MapModel, 'DEFAULT_PRIMARY_KEY', {
    value: "id"
  });
  MapModel.prototype = Object.create(ChangeModel.prototype, {
    constructor: {
      writable: true,
      value: MapModel
    },
    length: {
      get: function get() {
        return this._map.length;
      }
    },
    primaryKey: {
      get: function get() {
        return this._primaryKey;
      },
      set: function set(key) {
        if (key === this._primaryKey) {
          return;
        }
        this._primaryKey = !(key instanceof String || typeof key === 'string') || key === "" ? MapModel.DEFAULT_PRIMARY_KEY : key;
        if (this._map.length > 0) {
          this._map.clear();
        }
      }
    },
    add: {
      value: function value(entry) {
        if (entry === null || entry === undefined) {
          throw new ReferenceError(this + " add method failed, the passed-in argument not must be 'null'.");
        }
        this.validate(entry);
        if (this._primaryKey in entry) {
          if (!this._map.has(entry[this._primaryKey])) {
            this._map.set(entry[this._primaryKey], entry);
            this.notifyAdd(entry);
          } else {
            throw new ReferenceError(this + " add method failed, the passed-in entry is already register in the model with the specified primary key, you must remove this entry before add a new entry.");
          }
        } else {
          throw new ReferenceError(this + " add method failed, the entry is not identifiable and don't contains a primary key with the name '" + this._primaryKey + "'.");
        }
      }
    },
    clear: {
      value: function value() {
        this._map.clear();
        ChangeModel.prototype.clear.call(this);
      }
    },
    get: {
      value: function value(key) {
        return this._map.get(key);
      }
    },
    getByProperty: {
      value: function value(propName, _value) {
        if (propName === null || !(propName instanceof String || typeof propName === 'string')) {
          return null;
        }
        var datas = this._map.values();
        var size = datas.length;
        try {
          if (size > 0) {
            while (--size > -1) {
              if (datas[size][propName] === _value) {
                return datas[size];
              }
            }
          }
        } catch (er) {
        }
        return null;
      }
    },
    has: {
      value: function value(entry) {
        return this._map.hasValue(entry);
      }
    },
    hasByProperty: {
      value: function value(propName, _value2) {
        if (propName === null || !(propName instanceof String || typeof propName === 'string')) {
          return false;
        }
        var datas = this._map.values();
        var size = datas.length;
        if (size > 0) {
          while (--size > -1) {
            if (datas[size][propName] === _value2) {
              return true;
            }
          }
        }
        return false;
      }
    },
    hasKey: {
      value: function value(key) {
        return this._map.has(key);
      }
    },
    isEmpty: {
      value: function value() {
        return this._map.isEmpty();
      }
    },
    iterator: {
      value: function value() {
        return this._map.iterator();
      }
    },
    keyIterator: {
      value: function value() {
        return this._map.keyIterator();
      }
    },
    notifyAdd: {
      value: function value(entry) {
        if (!this.isLocked()) {
          this.added.emit(entry, this);
        }
      }
    },
    notifyRemove: {
      value: function value(entry) {
        if (!this.isLocked()) {
          this.removed.emit(entry, this);
        }
      }
    },
    notifyUpdate: {
      value: function value(entry) {
        if (!this.isLocked()) {
          this.updated.emit(entry, this);
        }
      }
    },
    remove: {
      value: function value(entry) {
        if (entry === null || entry === undefined) {
          throw new ReferenceError(this + " remove method failed, the entry passed in argument not must be null.");
        }
        if (this._primaryKey in entry) {
          if (this._map.has(entry[this._primaryKey])) {
            this._map.delete(entry[this._primaryKey]);
            this.notifyRemove(entry);
          } else {
            throw new ReferenceError(this + " remove method failed, no entry register in the model with the specified primary key.");
          }
        } else {
          throw new ReferenceError(this + " remove method failed, the entry is not identifiable and don't contains a primary key with the name '" + this._primaryKey + "'.");
        }
      }
    },
    setMap: {
      value: function value(map) {
        this._map = map instanceof KeyValuePair ? map : new ArrayMap();
      }
    },
    update: {
      value: function value(entry) {
        if (this._primaryKey in entry) {
          if (this._map.has(entry[this._primaryKey])) {
            this._map.set(entry[this._primaryKey], entry);
            this.notifyUpdate(entry);
          } else {
            throw new ReferenceError(this + " update method failed, no entry register in the model with the specified primary key.");
          }
        } else {
          throw new ReferenceError(this + " update method failed, the entry is not identifiable and don't contains a primary key with the name '" + this._primaryKey + "'.");
        }
      }
    },
    toMap: {
      value: function value() {
        return this._map;
      }
    }
  });

  function InitMapModel() {
    var model = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var datas = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var autoClear = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var autoSelect = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
    var autoDequeue = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
    var cleanFirst = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : false;
    Action.call(this);
    Object.defineProperties(this, {
      autoClear: {
        value: autoClear === true,
        writable: true
      },
      autoDequeue: {
        value: autoDequeue === true,
        writable: true
      },
      autoSelect: {
        value: autoSelect === true,
        writable: true
      },
      cleanFirst: {
        value: cleanFirst === true,
        writable: true
      },
      datas: {
        value: datas instanceof Array ? datas : null,
        writable: true
      },
      first: {
        value: null,
        writable: true
      },
      model: {
        value: model instanceof MapModel ? model : null,
        writable: true
      }
    });
  }
  InitMapModel.prototype = Object.create(Action.prototype, {
    constructor: {
      writable: true,
      value: InitMapModel
    },
    clone: {
      writable: true,
      value: function value() {
        return new InitMapModel(this.models, this.datas, this.autoClear, this.autoSelect, this.autoDequeue, this.cleanFirst);
      }
    },
    filterEntry: {
      writable: true,
      value: function value(_value) {
        return _value;
      }
    },
    reset: {
      writable: true,
      value: function value() {
        this.datas = null;
      }
    },
    run: {
      writable: true,
      value: function value() {
        this.notifyStarted();
        if (!(this.model instanceof MapModel)) {
          this.notifyFinished();
          return;
        }
        if (this.autoClear === true && !this.model.isEmpty()) {
          this.model.clear();
        }
        if (arguments.length > 0) {
          this.datas = (arguments.length <= 0 ? undefined : arguments[0]) instanceof Array ? arguments.length <= 0 ? undefined : arguments[0] : null;
        }
        if (!(this.datas instanceof Array) || this.datas.length === 0) {
          this.notifyFinished();
          return;
        }
        var entry;
        var size = this.datas.length;
        for (var i = 0; i < size; i++) {
          entry = this.filterEntry(this.datas[i]);
          this.model.add(entry);
          if (this.first === null && entry !== null) {
            this.first = entry;
          }
        }
        if (this.datas && this.datas instanceof Array && this.autoDequeue === true) {
          this.datas.length = 0;
        }
        if (this.first !== null && this.autoSelect === true) {
          if (this.model.has(this.first)) {
            this.model.current = this.model.get(this.first);
          } else {
            this.model.current = this.first;
          }
          if (this.cleanFirst === true) {
            this.first = null;
          }
        }
        this.notifyFinished();
      }
    }
  });

  /**
   * The {@link system.models} library provides a simple <b>MVC</b> implementation with a collection of <code>Model</code> classes to manage your applications.
   * @summary The {@link system.models} library provides a simple <b>MVC</b> implementation with a collection of <code>Model</code> classes to manage your applications.
   * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
   * @author Marc Alcaraz <ekameleon@gmail.com>
   * @namespace system.models
   * @memberof system
   * @example
   * var beforeChanged = function( value , model )
   * {
   *     trace( "before:" + value + " current:" + model.current ) ;
   * }
   *
   * var changed = function( value , model )
   * {
   *     trace( "change:" + value + " current:" + model.current ) ;
   * }
   *
   * var cleared = function( model )
   * {
   *     trace( "clear current:" + model.current ) ;
   * }
   *
   * var model = new ChangeModel() ;
   *
   * model.beforeChanged.connect( beforeChanged ) ;
   * model.changed.connect( changed ) ;
   * model.cleared.connect( cleared ) ;
   *
   * model.current = "hello" ;
   * model.current = "world" ;
   * model.current = null ;
   */
  var data = {
    ChangeModel: ChangeModel,
    MemoryModel: MemoryModel,
    Model: Model,
    arrays: {
      ArrayModel: ArrayModel
    },
    maps: {
      InitMapModel: InitMapModel,
      MapModel: MapModel
    }
  };

  var skip = false ;
  function sayHello( name = '' , version = '' , link = '' )
  {
      if( skip )
      {
          return ;
      }
      try
      {
          if ( navigator && navigator.userAgent && navigator.userAgent.toLowerCase().indexOf('chrome') > -1)
          {
              const args = [
                  `\n %c %c %c ${name} ${version} %c %c ${link} %c %c\n\n`,
                  'background: #ff0000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'color: #F7FF3C; background: #000000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'color: #F7FF3C; background: #ff0000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'background: #ff0000; padding:5px 0;',
              ];
              window.console.log.apply( console , args );
          }
          else if (window.console)
          {
              window.console.log(`${name} ${version} - ${link}`);
          }
      }
      catch( error )
      {
      }
  }
  function skipHello()
  {
      skip = true ;
  }

  var metas = Object.defineProperties({}, {
    name: {
      enumerable: true,
      value: 'vegas-js-models'
    },
    description: {
      enumerable: true,
      value: 'A library who contains classes and tools that provides extra <code>models</code> methods and implementations'
    },
    version: {
      enumerable: true,
      value: '1.0.1'
    },
    license: {
      enumerable: true,
      value: "MPL-2.0 OR GPL-2.0+ OR LGPL-2.1+"
    },
    url: {
      enumerable: true,
      value: 'https://bitbucket.org/ekameleon/vegas-js-models'
    }
  });
  var bundle = _objectSpread({
    metas: metas,
    sayHello: sayHello,
    skipHello: skipHello
  }, data);
  try {
    if (window) {
      window.addEventListener('load', function load() {
        window.removeEventListener("load", load, false);
        sayHello(metas.name, metas.version, metas.url);
      }, false);
    }
  } catch (ignored) {}

  return bundle;

})));
//# sourceMappingURL=vegas.models.js.map
