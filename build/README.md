# VEGAS JS - MODELS

**Vegas JS MODELS** - **version {{ version }}** is an *Opensource* library who contains classes and tools that provides extra <code>models</code> methods and implementations.

### About

 * Author : Marc ALCARAZ (aka eKameleon) - Creative Technologist and Digital Architect
 * Mail : ekameleon[at]gmail.com
 * LinkedIn : [https://www.linkedin.com/in/ekameleon/](https://www.linkedin.com/in/ekameleon/)

### License

Under tree opensource licenses :

 * [License MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/)
 * [License GPL 2.0+](https://www.gnu.org/licenses/gpl-2.0.html)
 * [License LGPL 2.1+](https://www.gnu.org/licenses/lgpl-2.1.html)

## Resources

#### ⌜ Download

Download on **Bitbucket** the latest code, report an issue, ask a question or contribute :

 * [https://bitbucket.org/ekameleon/vegas-js-models](https://bitbucket.org/ekameleon/vegas-js-models)

#### ⌜ Documentation

Get started with the the **Vegas JS** API :

 * [https://vegasjs.ooopener.com/](https://vegasjs.ooopener.com/)

#### ⌜ Slack Community

![slack-logo-vector-download.jpg](https://bitbucket.org/repo/AEbB9b/images/3509366499-slack-logo-vector-download.jpg)

Send us your email to join the **VEGAS** community on Slack !

## Install

#### ⌜ YARN / NPM

You can install VEGAS JS with [NPM](https://www.npmjs.com/package/vegas-js) or [Yarn](https://yarnpkg.com/).

```
$ yarn add vegas-js-models --dev
```

or

```
$ npm install vegas-js-models --save-dev
```

## Building and test the libraries

This library use [Yarn](https://yarnpkg.com/) with a serie of powerful packages (Babel, Mocha, etc.) to compile and build this library.

#### ⌜ Simple Build

1 - The first time, initialize the project and run yarn :
```
$ yarn
```

2 - Test + compile all the libraries :
```
$ yarn build
```

#### ⌜ VEGAS (only) Build

1 - Build the **./dist/vegas-models.js** : not minified + no comments + sourcemap.
```
$ yarn dev
```

2 - Build the **./dist/vegas-models.js** and watch the changing into the **./src** folder.
```
$ yarn watch
```

3 - Build the **./dist/vegas-models.min.js** : minified + no comments.
```
$ yarn prod
```

4 - Build the **./dist/vegas-models.min.js** and the **./dist/vegas-core.js** libraries only.
```
$ yarn start
```
#### ⌜ Examples

To launch the HTML examples, use the command :
```
$ yarn example ./examples/system/models/index.html
```

This command launch with the **BrowserSync** tool the html page of the example in your browser.

#### ⌜ Unit tests

We use the [Mocha](https://mochajs.org) and the Chai (http://chaijs.com/) tools to run the unit tests of the VEGAS JS libraries.

1 - Run all unit tests
```
$ yarn test
```

2 - Run a specific library, use one of this command :

The **--match** option trigger the unit test engine (based on **[Mocha](https://mochajs.org/)**) to only run tests matching the given pattern which is internally compiled to a RegExp, for examples :

```
$ yarn test -g system.models
```
Run all the unit tests.

```
$ yarn test -g system.models...
```
Run only the system.models specific unit tests.

The **--reporter** option define the unit test result rendering in the terminal with the values : 'spec', 'dot', 'landing', 'dot', 'nyan', 'list', 'mochawesome'. By default the 'spec' value is used.

```
$ yarn test --reporter nyan
```
![nyan.png](https://bitbucket.org/repo/E9RjA6/images/3930502565-nyan.png)

#### ⌜ Generates the documentation

The documentation of the framework is based on [JSDoc](http://usejsdoc.org/).

Run the documentation build with gulp :
```
$ yarn doc
```

The documentation is generated in the folder : **./docs/bin**

## History

 * 1998 : Flash
 * 2000 : First framework concept and first libraries (components, tools, design patterns)
 * 2004 : First official SVN repository
 * 2007 : Fusion with the Maashaack framework (eden, etc.)
 * 2015 : Google Code must die - **VEGAS** move from an old Google Code SVN repository to this Bitbucket GIT repository and REBOOT this source code.
 * 2016 : Begin the new JS architecture of the VEGAS JS library based on ES6
 * 2018 : Cut the JS packages of VEGAS in a set of independent libraries.
 