"use strict" ;

import ChangeModel from './src/ChangeModel'
import MemoryModel from './src/MemoryModel'
import Model       from './src/Model'

import ArrayModel   from './src/arrays/ArrayModel'
import InitMapModel from './src/maps/InitMapModel'
import MapModel     from './src/maps/MapModel'

/**
 * The {@link system.models} library provides a simple <b>MVC</b> implementation with a collection of <code>Model</code> classes to manage your applications.
 * @summary The {@link system.models} library provides a simple <b>MVC</b> implementation with a collection of <code>Model</code> classes to manage your applications.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.models
 * @memberof system
 * @example
 * var beforeChanged = function( value , model )
 * {
 *     trace( "before:" + value + " current:" + model.current ) ;
 * }
 *
 * var changed = function( value , model )
 * {
 *     trace( "change:" + value + " current:" + model.current ) ;
 * }
 *
 * var cleared = function( model )
 * {
 *     trace( "clear current:" + model.current ) ;
 * }
 *
 * var model = new ChangeModel() ;
 *
 * model.beforeChanged.connect( beforeChanged ) ;
 * model.changed.connect( changed ) ;
 * model.cleared.connect( cleared ) ;
 *
 * model.current = "hello" ;
 * model.current = "world" ;
 * model.current = null ;
 */
export default
{
    // classes

    ChangeModel : ChangeModel,
    MemoryModel : MemoryModel,
    Model       : Model,

    // packages

    /**
     * @namespace system.models.arrays
     * @memberof system.models
     */
    arrays : { ArrayModel },

    /**
     * @namespace system.models.maps
     * @memberof system.models
     */
    maps :
    {
        InitMapModel ,
        MapModel
    }
} ;