'use strict'

import Receiver from "vegas-js-signals/src/Receiver"

export default function MockSlot()
{
    Receiver.call( this );

    Object.defineProperties( this ,
    {
        /**
         * @private
         */
        _received : { value : false , writable : true },

        /**
         * @private
         */
        _values : { value : null , writable : true }
    });
}

MockSlot.prototype = Object.create( Receiver.prototype ,
{
    constructor : { writable : true , value : MockSlot } ,

    getValues : { value : function()
    {
        return this._values;
    }},

    isReceived : { value : function ()
    {
        return this._received;
    }},

    receive : { value : function ( /*Arguments*/ )
    {
        let values = Object.setPrototypeOf( arguments , Array.prototype ) ;

        this._received = true ;
        this._values   = values ;
    }},

    reset : { value : function ()
    {
        this._received = false ;
        this._values   = null ;
    }}
});
