# VEGAS JS NUMERIC OpenSource library - Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.1] - 2020-03-17
### Changed
- package.json : "vegas-js-core": "1.0.19",
- package.json : "vegas-js-data": "1.0.7",
- package.json : "vegas-js-process": "1.0.4",
- package.json : "vegas-js-signals": "1.3.6",
- package.json : "vegas-js-system-errors": "1.0.2",

## [1.0.0] - 2018-11-08
### Added
* First version

